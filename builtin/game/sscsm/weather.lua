--[[
	From Weather mod
	Copyright (C) Jeija (2013)
	Copyright (C) HybridDog (2015)
	Copyright (C) theFox6 (2018)
	Copyright (C) MultiCraft Development Team (2019-2020)

	This program is free software; you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation; either version 3.0 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License along
	with this program; if not, write to the Free Software Foundation, Inc.,
	51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
]]

local vmultiply, vadd = vector.multiply, vector.add
local random = math.random

weather = {
	type = "none",
	wind = {x = 0, y = 0, z = 0},
	registered = {},
	liquids = {}
}

--
-- Change of weather
--

sscsm.register_on_com_receive("builtin:weather", function(msg)
	for k, v in pairs(msg) do
		weather[k] = v
	end
end)

--
-- Processing players
--

assert(minetest.localplayer)
sscsm.every(0.1, function()
	local current_downfall = weather.registered[weather.type]
	if current_downfall == nil then return end

	local ppos = vector.round(minetest.localplayer:get_pos())

	ppos.y = ppos.y + 1.5
	-- Higher than clouds
	local cloud_height = weather.cloud_height
	cloud_height = cloud_height ~= 0 and cloud_height or 120
	if not core.is_valid_pos(ppos) or ppos.y > cloud_height or ppos.y < -8 then return end

	-- Inside liquid
	local head_inside = core.get_node_or_nil(ppos)
	if head_inside and weather.liquids[head_inside.name] then return end

	-- Too dark, probably not under the sky
	if core.get_node_light then
		local light = core.get_node_light(ppos, 0.5)
		if light and light < 12 then return end
	end

	local wind_pos = vmultiply(weather.wind, -1)
	local minp = vadd(vadd(ppos, {x = -8, y = current_downfall.height, z = -8}), wind_pos)
	local maxp = vadd(vadd(ppos, {x =  8, y = current_downfall.height, z =  8}), wind_pos)
	local vel = {x = weather.wind.x, y = -current_downfall.falling_speed, z = weather.wind.z}
	local vert = current_downfall.vertical or false

	core.add_particlespawner({
		amount = current_downfall.amount,
		time = 0.1,
		minpos = minp,
		maxpos = maxp,
		minvel = vel,
		maxvel = vel,
		minsize = current_downfall.size,
		maxsize = current_downfall.size,
		collisiondetection = true,
		collision_removal = true,
		vertical = vert,
		texture = current_downfall.texture,
		glow = 1
	})
end)
